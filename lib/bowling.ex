defmodule Bowling do
  def score(game) do
    score(game, 0)
  end
  defp score([[a,b]|rgame], acc) when a + b == 10 do
    [c|_d] = hd(rgame)
    score(rgame, acc + a + b + c)
  end
  defp score([frame|rgame], acc) do
    score(rgame, acc + score_frame(frame))
  end
  defp score([], acc), do: acc
  defp score_frame([a,b]), do: a + b
  defp score_frame([a,b,_c]), do: a + b
end
